﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Model
{
    public class AddressResponse
    {
        [JsonProperty("suggestions")]
        public List<SmartyAddress> Addresses { get; set; }
    }

    public class SmartyAddress
    {
        [JsonProperty("street_line")]
        public string StreetLine { get; set; }

        [JsonProperty("secondary")]
        public string Secondary { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zipcode")]
        public string Zipcode { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }

        [JsonProperty("countyFips")]
        public string CountyFips { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }
        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("rdi")]
        public string Rdi { get; set; }
    }
}
