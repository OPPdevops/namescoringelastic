﻿using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Model.Entity
{

    [Table("Name")]
    public class EstateName
    {
        [Key, Identity]
        public long Id { get; set; }

        [Column("Text")]
        public string Name { get; set; }

        public bool? IsIndexed { get; set; }

    }
}
