using ElasticSearch.Common.Scheduler.Scheduling;
using ElasticSearch.Services;
using ElasticSearch.Services.Interfaces;
using ElasticSearch.Services.Repository;
using ElasticSearch.Services.Repository.Base;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.Config;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //https://github.com/phnx47/dapper-repositories

            //Your DB Provider
            //==============================================
            MicroOrmConfig.SqlProvider = SqlProvider.MSSQL;
            //Add generic SqlGenerator as singleton
            services.AddSingleton(typeof(ISqlGenerator<>), typeof(SqlGenerator<>));
            //Your db factory
            services.AddSingleton<IDbFactory, DbFactory>(x => new DbFactory(Configuration["AppSettings:ConnectionString"]));
            services.AddSingleton(typeof(IDapperRepository<>), typeof(DapperRepository<>));
            //==============================================

            //==============================================
            services.AddSingleton<IEstateRepository, EstateRepository>();
            services.AddSingleton<IJobExecutionLogRepository, JobExecutionLogRepository>();
            services.AddSingleton<IElasticSearchService, ElasticSearchService>();
            //==============================================

            // Add scheduled tasks & scheduler
            //==============================================
            services.AddSingleton<IScheduledTask, EstateBackgroundJob>();
            services.AddScheduler((sender, args) =>
            {
                Console.Write(args.Exception.Message);
                args.SetObserved();
            });
            //==============================================

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
