﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Common.HealthCheck
{
    public class ProcessHealthCheck : ISupportHealthcheck
    {
        private static readonly Process CurrentProcess = Process.GetCurrentProcess();

        public string ComponentName => typeof(ProcessHealthCheck).Name;

        private SpinLock _lock = new SpinLock(false);
        private long _maxMemoryBytes;

        public Task<HealthcheckResult> PerformHealthcheck(CancellationToken cancellationToken = default)
        {
            long totalMemoryBytes = GC.GetTotalMemory(false);
            var maxMemoryBytes = totalMemoryBytes;

            // save max memory
            var lockWasTaken = false;
            _lock.Enter(ref lockWasTaken);
            if (totalMemoryBytes > _maxMemoryBytes)
                _maxMemoryBytes = totalMemoryBytes;
            else
                maxMemoryBytes = _maxMemoryBytes;
            if (lockWasTaken)
                _lock.Exit();

            DateTime startTime = CurrentProcess.StartTime;

            if (startTime.Kind != DateTimeKind.Utc)
                startTime = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc);

            HealthcheckResult healthcheckResult = new HealthcheckResult
            {
                ComponentName = ComponentName,
                IsHealthy = true,
                HealthDescription = "Process and system details",
                Detail = new
                {
                    OS = System.Runtime.InteropServices.RuntimeInformation.OSDescription,
                    ProcessName = CurrentProcess.ProcessName,
                    ProcessStartTime = startTime,
                    ProcessUpTime = DateTime.UtcNow.Subtract(startTime),
                    MachineName = Environment.MachineName,
                    MachineUpTime = new TimeSpan(Environment.TickCount),
                    ProcessorCount = Environment.ProcessorCount,
                    ProcessThreadCount = CurrentProcess.Threads.Count,
                    ProcessTotalProcessorTime = CurrentProcess.TotalProcessorTime,
                    totalMemoryBytes = totalMemoryBytes,
                    totalMemoryMB = totalMemoryBytes / 1024 / 1024,
                    maximumAllocatedMemoryBytes = maxMemoryBytes,
                    maximumAllocatesMemoryMB = maxMemoryBytes / 1024 / 1024,
                    gcCollections = new[] { GC.CollectionCount(0), GC.CollectionCount(1), GC.CollectionCount(2) }
                },
            };

            return Task.FromResult(healthcheckResult);
        }
    }

}
