﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Common.HealthCheck
{
    public class HealthcheckResult
    {
        public string ComponentName { get; set; }

        /// <summary>
        ///     Component is fully initialized and ready to use.
        /// </summary>
        public bool IsReady { get; set; } = true;

        public bool IsHealthy { get; set; }
        public string MachineName { get; set; }
        public string EnvironmentName { get; set; }
        public string HealthDescription { get; set; }
        public object Detail { get; set; }

        public HealthcheckResult()
        {
            MachineName = Environment.MachineName;
        }
    }

    public class Boxed<T>
    {
        public T Value;

        /// <inheritdoc />
        public Boxed(T value)
        {
            Value = value;
        }
    }
}
