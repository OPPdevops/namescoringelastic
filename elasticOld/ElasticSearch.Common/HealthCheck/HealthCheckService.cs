﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Common.HealthCheck
{
    public class HealthCheckService : IHealthCheckService
    {
        private static readonly Process CurrentProcess = Process.GetCurrentProcess();
        public string ComponentName => typeof(HealthCheckService).Name;
        private SpinLock spinLock = new SpinLock(false);
        private long maxMemoryBytes;

        public Task<HealthCheckModel> PerformHealthcheck(CancellationToken cancellationToken = default)
        {
            long totalMemoryBytes = GC.GetTotalMemory(false);
            long maxMemoryBytes = totalMemoryBytes;

            // save max memory
            bool lockWasTaken = false;
            spinLock.Enter(ref lockWasTaken);

            if (totalMemoryBytes > this.maxMemoryBytes)
                this.maxMemoryBytes = totalMemoryBytes;
            else
                maxMemoryBytes = this.maxMemoryBytes;

            if (lockWasTaken)
                spinLock.Exit();

            DateTime startTime = CurrentProcess.StartTime;
            if (startTime.Kind != DateTimeKind.Utc)
                startTime = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc);

            var healthcheck = new HealthCheckModel
            {
                ComponentName = ComponentName,
                IsHealthy = true,
                HealthDescription = "Process and system details",
                Detail = new
                {
                    OS = System.Runtime.InteropServices.RuntimeInformation.OSDescription,
                    CurrentProcess.ProcessName,
                    ProcessStartTime = startTime,
                    ProcessUpTime = DateTime.UtcNow.Subtract(startTime),
                    Environment.MachineName,
                    MachineUpTime = new TimeSpan(Environment.TickCount),
                    Environment.ProcessorCount,
                    ProcessThreadCount = CurrentProcess.Threads.Count,
                    ProcessTotalProcessorTime = CurrentProcess.TotalProcessorTime,
                    totalMemoryBytes,
                    totalMemoryMB = totalMemoryBytes / 1024 / 1024,
                    maximumAllocatedMemoryBytes = maxMemoryBytes,
                    maximumAllocatesMemoryMB = maxMemoryBytes / 1024 / 1024,
                    gcCollections = new[] { GC.CollectionCount(0), GC.CollectionCount(1), GC.CollectionCount(2) }
                },
            };

            return Task.FromResult(healthcheck);
        }
    }
}
