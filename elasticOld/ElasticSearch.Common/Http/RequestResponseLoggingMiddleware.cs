﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Common.Http
{
    /// <summary>
    /// Logging All User requests. Usage: app.UseMiddleware<RequestResponseLoggingMiddleware>();
    /// </summary>
    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestResponseLoggingMiddleware> _logger;
        private string _guid;

        public RequestResponseLoggingMiddleware(RequestDelegate next, ILogger<RequestResponseLoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.Value.Contains("/api/healthcheck"))
            {
                //First, get the incoming request
                await FormatRequest(context.Request);

                //Copy a pointer to the original response body stream
                var originalBodyStream = context.Response.Body;

                //Create a new memory stream
                using (var responseBody = new MemoryStream())
                {
                    //use that for the temporary response body
                    context.Response.Body = responseBody;

                    //Continue down the Middleware pipeline, eventually returning to this class
                    await _next(context);

                    //Format the response from the server
                    await FormatResponse(context.Response);

                    //TODO: Save log to chosen datastore

                    //Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
            else
                await _next(context);
        }

        private async Task FormatRequest(HttpRequest request)
        {
            var body = request.Body;
            if (!request.Headers.TryGetValue("x-request-id", out StringValues requestId))
                requestId = Guid.NewGuid().ToString();
            else
            {
                if (!Guid.TryParse(requestId.ToString(), out Guid parsedRequestId))
                {
                    request.HttpContext.Response.StatusCode = 400; //Bad Request
                    await request.HttpContext.Response.WriteAsync("Bad Request: x-request-id passed is not a valid guid");
                    return;
                }
            }

            _guid = requestId;

            //This line allows us to set the reader for the request back at the beginning of its stream.
            request.EnableBuffering();

            //Read the request stream.  First, we create a new byte[] with the same length as the request stream
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];

            //Then we copy the entire request stream into the new buffer.
            await request.Body.ReadAsync(buffer, 0, buffer.Length);

            //We convert the byte[] into a string using UTF8 encoding...
            var bodyAsText = Encoding.UTF8.GetString(buffer);

            //finally, assign the read body back to the request body, which is allowed because of EnableRewind()
            request.Body.Seek(0, SeekOrigin.Begin);
            _logger.LogInformation($"Request: {request.Scheme} {request.Host}{request.Path} {request.QueryString} {requestId} {bodyAsText}");
        }

        private async Task FormatResponse(HttpResponse response)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //Copy it into a string
            string bodyAsText = await new StreamReader(response.Body).ReadToEndAsync();

            response.Headers.Add("x-request-id", _guid);

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);
            _logger.LogInformation($"API Response: {response.StatusCode}: {_guid} {bodyAsText}");
        }
    }
}
