﻿using Elasticsearch.Net;
using ElasticSearch.Model.Elastic;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Services
{
    public class ElasticSearchService : IElasticSearchService
    {
        private readonly ElasticClient client;
        private readonly string defaultIndex = "custom-estates";
        public ElasticSearchService()
        {
            var settings = new ConnectionSettings(new Uri("http://localhost:9200"))
                                .DefaultIndex(defaultIndex)
                                .DisableDirectStreaming();

            client = new ElasticClient(settings);
        }
        public void DeleteIndexData()
        {
            client.Indices.Delete(defaultIndex);
        }
        public void CreateIndex()
        {
            // to allow the example to be re-run
            if (client.Indices.Exists(defaultIndex).Exists)
                client.Indices.Delete(defaultIndex);

            var response = client.Indices.Create(defaultIndex, index => index
                .Settings(s => s
                    .NumberOfShards(1)
                    .NumberOfReplicas(0)
                    .Analysis(a => a
                        .Analyzers(an => an
                            .Custom("exact_estate", ca => ca
                                .CharFilters("convert_abbreviations", "remove_apostrophes")
                                .Tokenizer("keyword")
                                .Filters("lowercase", "synonym_abbreviations")
                            )
                            .Custom("standard_estate", ca => ca
                                .CharFilters("convert_abbreviations", "remove_apostrophes")
                                .Tokenizer("standard")
                                .Filters("lowercase", "synonym_abbreviations")
                            )
                        )
                        .TokenFilters(tf => tf
                            .Synonym("synonym_abbreviations", tf => tf
                                .Synonyms(CompanyAbbreviations())
                                .Tokenizer("whitespace")))
                        .CharFilters(cf => cf
                            .Mapping("convert_abbreviations", mf => mf
                                //.Mappings(CompanyAbbreviations())
                                .Mappings("& => and")
                            )
                            .Mapping("remove_apostrophes", mf => mf
                                .Mappings(
                                    "\\u0091=>",
                                    "\\u0092=>",
                                    "\\u2018=>",
                                    "\\u2019=>",
                                    "\\u201B=>",
                                    "\\u0027=>"
                                )
                            )
                        )
                    )
                )
                .Map<Estate>(x => x.AutoMap()
                .Properties(p => p
                .Text(t => t
                    .Name(n => n.EstateName)
                    .Norms(false)
                    .Analyzer("standard_estate")
                    .Fields(f => f
                        .Text(tt => tt
                            .Name("exact_search")
                            .Analyzer("exact_estate")
                            .Norms(false)
                        )
                    )
                ))));
        }

        //public void IndexData()
        //{
        //    var data = GetAll();
        //    client.Bulk(b => b.IndexMany(data).Refresh(Refresh.WaitFor));
        //}
        public void IndexData(List<Estate> data)
        {
            client.Bulk(b => b.IndexMany(data).Refresh(Refresh.WaitFor));
        }
        public IEnumerable<MatchDocument> SearchExact(string term)
        {
             var response = client.Search<Estate>(s => s
                 .Query(q => q
                     .Match(m => m
                     .Field(f => f.EstateName.Suffix("exact_search"))
                     .Query(term)
             )));

            return response.Hits.Select(h => new MatchDocument
            {
                Score = h.Score,
                Name = h.Source.EstateName
            });
        }

        public IEnumerable<MatchDocument> SearchFuzzy(string term)
        {
            var response = client.Search<Estate>(s => s
                  .Explain() //comment in production.
                  .Query(q => q
                      .Match(m => m
                      .Field(f => f.EstateName)
                      .Query(term)
                      .Fuzziness(Fuzziness.Auto)
                      //.Boost(1.1)
                      .FuzzyRewrite(MultiTermQueryRewrite.ConstantScore)
              )));

            var multiplier = ScoreMultiplier(term);
            return response.Hits.Select(h => new MatchDocument
            {
                Score = h.Score * multiplier,
                Id =h.Source.EstateId,
                Name = h.Source.EstateName
            });


        }

        private double ScoreMultiplier(string term)
        {
            return (double)decimal.Divide(100, term.Split(" ").Length);
        }

        private string[] CompanyAbbreviations()
        {
            return new[] {  "co => company",
                            "co. => company",
                            "corp => corporation",
                            "corp. => corporation",
                            "inc => incorporated",
                            "inc. => incorporated",
                            "ltd => limited",
                            "ltd. => limited",
                            "bros => brothers",
                            "bros. => brothers",
                            "cie => compagnie",
                            "cie. => compagnie",
                            "mfg => manufacturing",
                            "mfg. => manufacturing",
                            "mfrs => manufacturers",
                            "mfrs. => manufacturers",

                            "l.l.c. => llc",
                            "limited liability company => llc",
                            "ltd. liability company => llc",
                            "limited liability co. => llc",
                            "ltd. liability co. => llc"};
        }
        private List<Estate> GetAll()
        {
            return new List<Estate>
            {
                new Estate { EstateId = 1, EstateName = "CHARLES SCHWAB", Address = "" },
                new Estate { EstateId = 2, EstateName = "SCHWAB CHARLES", Address = "" },
                new Estate { EstateId = 3, EstateName = "CHARLES SCHWAB COMPANY INC", Address = "" },
                new Estate { EstateId = 4, EstateName = "Schwabe, Charles T", Address = "" },
                new Estate { EstateId = 5, EstateName = "CHARLES SCHWAB & CO INC", Address = "" },
                new Estate { EstateId = 5, EstateName = "CHARLES SCHWAB & CO. INC", Address = "" },
                new Estate { EstateId = 5, EstateName = "CHARLES SCHWAB & CO. INCORPORATED", Address = "" },
                new Estate { EstateId = 5, EstateName = "CHARLES SCHWAB & COMPANY INCORPORATED", Address = "" },
                new Estate { EstateId = 5, EstateName = "CHARLES SCHWAB AND COMPANY INCORPORATED", Address = "" },
                new Estate { EstateId = 6, EstateName = "CHARLES SCHWAB & CO INC IRA", Address = "" },
                new Estate { EstateId = 7, EstateName = "SCHWAB ED", Address = "" },
                new Estate { EstateId = 8, EstateName = "SCHWAB, T", Address = "" },
                new Estate { EstateId = 9, EstateName = "CHARLES, H T", Address = "" },
                new Estate { EstateId = 10, EstateName = "SCHWAB RICHARD W", Address = "" },
                new Estate { EstateId = 11, EstateName = "Schwab, Howard F", Address = "" },
                new Estate { EstateId = 12, EstateName = "CHARLES A SCHAFFER, SCHAFFER CHARLES A", Address = "" },
                new Estate { EstateId = 13, EstateName = "COLE CHARLES W", Address = "" },
                new Estate { EstateId = 14, EstateName = "HARLEY CHARLES L", Address = "" },
                new Estate { EstateId = 15, EstateName = "CHAMBERS CHARLES E", Address = "" },
                new Estate { EstateId = 16, EstateName = "Schwab, Nancy A", Address = "" },
                new Estate { EstateId = 17, EstateName = "Miles, Charles W", Address = "" },
                new Estate { EstateId = 18, EstateName = "SCHWAB JEAN M", Address = "" },
                new Estate { EstateId = 19, EstateName = "Charles, O C", Address = "" },
                new Estate { EstateId = 20, EstateName = "MORALES, CHARLES", Address = "" },
                new Estate { EstateId = 21, EstateName = "SCHWAB, ROBERT", Address = "" },
                new Estate { EstateId = 22, EstateName = "CHARLES", Address = "" },
                new Estate { EstateId = 23, EstateName = "SCHAPP CHARLES", Address = "" },
                new Estate { EstateId = 24, EstateName = "SCHWEIGART", Address = "" },

                new Estate { EstateId = 25, EstateName = "LIMITED, PEPSICO HOLDING", Address = "" },
                new Estate { EstateId = 26, EstateName = "PEPSICO HOLDING, LIMITED", Address = "" },
                new Estate { EstateId = 27, EstateName = "Pepsico Holdings", Address = "" }
            };
        }
    }

    public interface IElasticSearchService
    {
        void DeleteIndexData();
        void IndexData(List<Estate> data);

        void CreateIndex();
        //void SearchWildcard(string term);
        IEnumerable<MatchDocument> SearchExact(string term);
        IEnumerable<MatchDocument> SearchFuzzy(string term);
    }
}
