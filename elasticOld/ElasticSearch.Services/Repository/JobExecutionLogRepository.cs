﻿using ElasticSearch.Model.Entity;
using ElasticSearch.Services.Interfaces;
using ElasticSearch.Services.Repository.Base;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Services.Repository
{

    public class JobExecutionLogRepository : BaseRepository<JobExecutionLog>, IJobExecutionLogRepository
    {
        private readonly ILogger<JobExecutionLogRepository> logger;
        public JobExecutionLogRepository(IDbFactory factory, ISqlGenerator<JobExecutionLog> sqlGenerator, ILogger<JobExecutionLogRepository> logger)
            : base(factory, sqlGenerator)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<bool> AddAsync(string jobName)
        {
            try
            {
                return await base.InsertAsync(new JobExecutionLog {JobName = jobName, CreatedAtUTC = DateTime.UtcNow});
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while adding data. Method: AddressUSPSRepository.AddAsync", ex);
                return false;
            }

        }
    }
}
