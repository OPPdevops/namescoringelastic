/* eslint-disable no-param-reassign */
import React, { PureComponent } from 'react';
import DatePicker from 'react-datepicker';
import MinusIcon from 'mdi-react/MinusIcon';
import PropTypes from 'prop-types';
import CalendarBlankIcon from 'mdi-react/CalendarBlankIcon';
import moment from 'moment';
class IntervalDatePickerField extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    meta: PropTypes.shape({
      touched: PropTypes.bool,
      error: PropTypes.string,
    }),
  };

  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      endDate: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChangeStart = startDate => this.handleChange({ startDate });

  handleChangeEnd = endDate => this.handleChange({ endDate });

  handleChange({ startDate, endDate }) {
    const { startDate: stateStartDate, endDate: stateEndDate } = this.state;

    startDate = startDate || stateStartDate;
    endDate = endDate || stateEndDate;

    if (moment(startDate).isAfter(endDate)) {
      endDate = startDate;
    }
    const { onChange } = this.props.input;
    if (onChange) {
      if (startDate) {
        onChange(startDate)
      } else {
        onChange(endDate)
      }
    }
    this.setState({ startDate, endDate });
    if (this.props.onChange) {
      this.props.onChange(startDate, endDate, this.props.name);
    }
  }

  updateDatePickerValues = () => {
    this.setState({
      startDate: null,
      endDate: null
    })
  }

  render() {
    const { startDate, endDate } = this.state;
    const { clear } = this.props;
    return (
      <div className="date-picker date-picker--interval">
        {/* <CalendarBlankIcon /> */}
        <DatePicker
          selected={clear ? this.updateDatePickerValues() : startDate}
          selectsStart
          startDate={startDate}
          endDate={endDate}
          onChange={this.handleChangeStart}
          dateFormat="dd-MM-yyyy"
          placeholderText="From Date"
          dropDownMode="select"
        />
        <MinusIcon className="date-picker__svg" />
        {/* <CalendarBlankIcon /> */}
        <DatePicker
          selected={clear ? this.updateDatePickerValues() : endDate}
          selectsEnd
          startDate={startDate}
          endDate={endDate}
          onChange={this.handleChangeEnd}
          dateFormat="dd-MM-yyyy"
          placeholderText="To Date"
          dropDownMode="select"
        />
      </div>
    );
  }
}

const renderIntervalDatePickerField = (props) => {
  const { input, meta } = props;
  return (
    <div>
      <IntervalDatePickerField
        {...input}
        onChange={props.handleDateValueInParent}
        input={input}
        clear={props.clear}
      />
      {meta.touched && meta.error && <span className="form__form-group-error">{meta.error}</span>}
    </div>
  );
};

renderIntervalDatePickerField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func,
  }),
};

export default renderIntervalDatePickerField;
