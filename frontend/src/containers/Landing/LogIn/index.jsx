import React from 'react';
import LogInForm from './components/LogInForm';
import config from '../../../config/config';
import configImages from '../../../config/configImages';
import LogoUI from '../../LogoUI/logoUI'
const LogIn = () => (
  <div className="account">
    <div className="account__wrapper">
      <div className="account__card p-2">
        <div className='row justifyContentLogin'>
          <div className='col-sm-5 textcenter mt-4 pl-0 pr-0 mb-4'>
            <LogoUI />
            <img src={configImages.loginImage} className='hide-mobile img_htwt'></img>
          </div>
          <div className='col-sm-6 vertical_middle width_90'>
            <div className='width_90'>
              <LogInForm onSubmit />
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
);

export default LogIn;
