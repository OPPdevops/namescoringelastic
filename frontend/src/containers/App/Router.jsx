/* eslint indent: "off" */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MainWrapper from './MainWrapper';
import Layout from '../Layout/index';

// Login
import Landing from '../Landing/LogIn';
// import LoginCheck from '../Landing/loginCheck';

// Dashboard
import Home from '../Dashboard/index';

import Activities from '../Cruds/Activities'

// Tables
import Employees from '../Cruds/Employees/index';
import Scorings from '../Cruds/Scorings/index';

import Settings from '../Cruds/Settings/index'
import AdminSettings from '../Settings/index'
// 404 page not found 
import ErrorNotFound from '../Account/404/index';

const Tables = () => {
  return <Switch>
    <Route path="/employees" component={Employees} />
    <Route path="/scorings" component={Scorings} />

    <Route path="/settings" component={Settings} />
    <Route path="/adminSettings" component={AdminSettings} />
    <Route path="/activities" component={Activities} />
  </Switch >
}


const wrappedRoutes = () => {
  let sidebar = {
    collapse: true,
    show: false
  }
  return <div>
    <Layout
      sidebar={sidebar}
    />
    <div className="container__wrap">
      <Route path="/" component={Scorings} />
    </div>
  </div>
}

const Router = () => (
  <MainWrapper>
    <main>
      <Switch>
        <Route exact path="/" component={wrappedRoutes} />

        <Route path="/" component={wrappedRoutes} />
        <Route component={ErrorNotFound} />

      </Switch>
    </main>
  </MainWrapper>
);

export default Router;
