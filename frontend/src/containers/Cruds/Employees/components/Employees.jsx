import React from 'react';
import DataTables from '../../CommonDataTable/DataTable';
import config from '../../../../config/config';
import apiCalls from '../../../../config/apiCalls'
import ViewModal from '../../CommonModals/viewModal';
import NewUserModal from '../../CommonModals/NewUserModal';

import FormModal from '../../../Form/FormModal';
import store from '../../../App/store';
import filePath from "../../../../config/configSampleFiles";
// config file
export default class Teachers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      languageData: ''
    };
  }

  static getDerivedStateFromProps(props, state) {
    let storeData = store.getState()
    let languageData = storeData && storeData.settingsData && storeData.settingsData.settings && storeData.settingsData.settings.languageTranslation ? storeData.settingsData.settings.languageTranslation : ""
    return { languageData: languageData };
  }

  getTableFields = () => {
    let { languageData } = this.state
    let roleTypes = [
      {
        label: 'All',
        value: null
      },
      {
        label: 'Admin',
        value: 'Admin'
      },
      {
        label: 'Super Admin',
        value: 'Super Admin'
      },
      {
        label: 'User',
        value: 'User'
      },
    ];
    let StatusTypes = [
      {
        label: 'All',
        value: null
      },
      {
        label: 'Active',
        value: 'Active'
      },
      {
        label: 'Inactive',
        value: 'Inactive'
      },
      {
        label: 'Pending',
        value: 'Pending'
      }
    ];
    let data = [{"textAlign":"center","width":47,"field":"","fieldType":"multiple","header":"","selectionMode":"multiple","show":true,"mobile":true,"displayInSettings":true},{"textAlign":"center","width":47,"field":"Sno","header":"SNo","filter":false,"sortable":false,"placeholder":"Search","show":true,"mobile":true,"displayInSettings":true},{"name":"name","type":"text","placeholder":"name","label":"name","id":"name","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"fieldType":"Link","textAlign":"Center","disabled":true,"show":true,"field":"name","header":"name","mobile":true,"displayInSettings":true,"style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"email","type":"email","placeholder":"email","label":"email","id":"email","displayinaddForm":"true","displayineditForm":"false","displayinlist":"true","controllerName":null,"textAlign":"Center","disabled":true,"show":true,"field":"email","header":"email","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"address","type":"textarea","placeholder":"address","label":"address","id":"address","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"disabled":true,"field":"address","header":"address","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"role","type":"text","placeholder":"role","label":"role","id":"role","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"field":"role","header":"role","disabled":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"phone","type":"text","placeholder":"phone","label":"phone","id":"phone","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"disabled":true,"field":"phone","header":"phone","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}}]
    return data;
  };

  getFormFields = () => {
    let { languageData } = this.state
    let statusTypes = [
      {
        label: 'Active',
        value: 'Active'
      },
      {
        label: 'Inactive',
        value: 'Inactive'
      },
      {
        label: 'Pending',
        value: 'Pending'
      },
    ];
    return ([{"name":"name","type":"text","placeholder":"name","label":"name","id":"name","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"fieldType":"Link","disabled":true,"show":true,"mobile":true,"displayInSettings":true,"style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"},"isAddFormHidden":false,"isEditFormHidden":false},{"name":"email","type":"email","placeholder":"email","label":"email","id":"email","displayinaddForm":"true","displayineditForm":"false","displayinlist":"true","controllerName":null,"disabled":true,"show":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"},"isAddFormHidden":false,"isEditFormHidden":true},{"name":"address","type":"textarea","placeholder":"address","label":"address","id":"address","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"show":true,"disabled":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"},"isAddFormHidden":false,"isEditFormHidden":false},{"name":"role","type":"text","placeholder":"role","label":"role","id":"role","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"show":true,"disabled":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"},"isAddFormHidden":false,"isEditFormHidden":false},{"name":"phone","type":"text","placeholder":"phone","label":"phone","id":"phone","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"show":true,"disabled":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"},"isAddFormHidden":false,"isEditFormHidden":false}]);
  }

  submit = async (item) => {

    this.setState({ isOpenFormModal: true })
    await this.formModalRef.getRowData(item, 'edit');

    console.log("Submit Button in sode          ")
  }
  getMobileTableFields = () => {
    let data = [{"textAlign":"center","width":47,"field":"","fieldType":"multiple","header":"","selectionMode":"multiple","show":true,"mobile":true,"displayInSettings":true},{"textAlign":"center","width":47,"field":"Sno","header":"SNo","filter":false,"sortable":false,"placeholder":"Search","show":true,"mobile":true,"displayInSettings":true},{"name":"name","type":"text","placeholder":"name","label":"name","id":"name","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"fieldType":"Link","textAlign":"Center","disabled":true,"show":true,"field":"name","header":"name","mobile":true,"displayInSettings":true,"style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"email","type":"email","placeholder":"email","label":"email","id":"email","displayinaddForm":"true","displayineditForm":"false","displayinlist":"true","controllerName":null,"textAlign":"Center","disabled":true,"show":true,"field":"email","header":"email","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"address","type":"textarea","placeholder":"address","label":"address","id":"address","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"disabled":true,"field":"address","header":"address","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"role","type":"text","placeholder":"role","label":"role","id":"role","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"field":"role","header":"role","disabled":true,"mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}},{"name":"phone","type":"text","placeholder":"phone","label":"phone","id":"phone","displayinaddForm":"true","displayineditForm":"true","displayinlist":"true","controllerName":null,"textAlign":"Center","show":true,"disabled":true,"field":"phone","header":"phone","mobile":true,"displayInSettings":true,"fieldType":"Link","style":{"color":"#0e4768","cursor":"pointer","textTransform":"capitalize"}}]

    return data;
  };
  closeFormModal = async () => {
    this.setState({
      isOpenFormModal: false,
    })
  }
  // submit1 = item => {
  //   let x = employeesFields
  //   let objemployees = {}

  //   objemployees["_id"] = item[0]["_id"]
  //   for (let x2 of x) {
  //     objemployees[x2] = item[0][x2]
  //   }
  //   let formFields = this.getFormFields()

  //   formFields = formFields.filter(y => x.includes(y.name))

  //   this.setState({
  //     openNewUserModal: true,
  //     item: objemployees,
  //     newFormFields: formFields
  //   });
  // }
  
  cancelReset = async (type) => {
    this.setState({
      openNewUserModal: false
    });
    window.location.reload();

    // if (type == "submit") {
    // type == "submit"
    //   ?
    // await this.dataTableRef.getDataFromServer(this.state.filterCriteria, "refresh");
    // }

    // : null;
  };
  render() {
    return (
      <span>
        <DataTables
          onRef={ref => (this.dataTableRef = ref)}
          // MobileTableFields={this.getMobileTableFields}
          // getTableFields={this.getTableFields}
          // formFields={this.getFormFields}
          // 
addRequired={true}
editRequired={true}
deleteRequired={true}
viewRequired={true}
exportRequired={true}
sample={true}
          // globalSearch={'Display Name/Email'}
          // type='Employees'
          // apiUrl={apiCalls.Employees}
          getTableFields={this.getTableFields}
          formFields={this.getFormFields}
          // exportRequried={true}

          printRequried={true}
          actionsTypes={[{
            'name': 'Delete',
            "options": [
              { 'label': 'Delete', 'value': 'Delete', 'show': true, "multiple": true, },
            ]
          },
          // {
          //   'name': 'Block',
          //   "options": [
          //     { 'label': 'Block', 'value': 'Block', 'show': true, "multiple": false, }
          //   ]
          // },
          {
            'name': 'ResetPassword',
            "options": [
              { 'label': 'ResetPassword', 'value': 'ResetPassword', 'show': true, "multiple": false, }
            ]
          },
          {
            'name': 'Submit',
            'action': this.submit,
            "options": [
              { 'label': 'Submit', 'value': 'Submit', 'show': true, "multiple": false },
            ]
          },
            // {
            //   name: "newModel",
            //   action: this.submit1,
            //   options: [
            //     { label: "newModel", value: "newModel", show: true, multiple: false }
            //   ]
            // }
            
          ]}
          // addRequried={insertAdd}
          // editRequired={true}
          // deleteRequired={true}
          // viewRequired={true}
          settingsRequired={true}
          filterRequired={true}
          gridRequried={true}
          exportToCsv={true}

          sampleFilePath={filePath.employees}

          globalSearch={'Display Name/Email'}
          type='Employees'
          routeTo={apiCalls.Employees}
          displayViewOfForm='screen'
          apiResponseKey={apiCalls.Employees}
          apiUrl={apiCalls.Employees}

        />
        {this.state.isOpenFormModal ? (
          <FormModal
            onRef={(ref) => (this.formModalRef = ref)}
            formType="edit"
            openFormModal={this.state.isOpenFormModal}
            formFields={this.props.formFields}
          />

        ) : null}
        {this.state.openNewUserModal ? (
          <NewUserModal
            openNewUserModal={this.state.openNewUserModal}
            cancelReset={this.cancelReset}
            item={this.state.item}
            newFormFields={this.state.newFormFields}
            recordId={this.state.item._id}
            entityType="Employees"
            apiUrl={apiCalls.Employees}
          />
        ) : null}
      </span>
    );
  }
}