import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Employees from './components/Employees';

const employees = ({ t }) => (
  <Container>
    <Employees />
  </Container>
);

employees.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation('common')(employees);
