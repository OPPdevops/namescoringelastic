const commonSampleFilePath =
  "http://localhost:3000/images/sample";

const configSampleFiles = { "employees": "http://localhost:4135/images/sample_employee_bulk_upload_file.csv", "Activitiess": "http://localhost:4135/images/sample_Activities_bulk_upload_file.csv", "Email Templatess": "http://localhost:4135/images/sample_Email Templates_bulk_upload_file.csv", "Roless": "http://localhost:4135/images/sample_Roles_bulk_upload_file.csv", "Upload Historys": "http://localhost:4135/images/sample_Upload History_bulk_upload_file.csv", "scorings": "http://localhost:4135/images/sample_scoring_bulk_upload_file.csv" };
export default configSampleFiles;