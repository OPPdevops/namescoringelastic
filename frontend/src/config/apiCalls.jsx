
const apiCalls = {
    "loginUser": "auth/login", "loginChangePassword": "auth/loginChangePassword", "MenuList": "menus", "Settings": "settings", "LocationImagePath": "uploads?uploadWhileCreate=true&uploadPath=location", "ProfileImagePath": "uploads?uploadWhileCreate=true&uploadPath=employee", "dashboard": "dashboard/counts", "Employee": "employees", "Employees": "employees", "Scoring": "scorings",
    "Scorings": "scorings",
    "Uploads": "bulkuploadStatus"
}
export default apiCalls;
