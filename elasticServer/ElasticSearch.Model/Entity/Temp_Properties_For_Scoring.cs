﻿using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElasticSearch.Model.Entity
{

    [Table("TempProperties")]
    public class PropertyName
    {
        [Key, Identity]
        //[Column("Id")]
        public long StatePropertyId { get; set; }

        [Column("Property_Owner")]
        public string Name { get; set; }

    }
}
