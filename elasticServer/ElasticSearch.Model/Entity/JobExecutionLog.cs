﻿using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ElasticSearch.Model.Entity
{
    [Table("JobExecutionLog")]
    public class JobExecutionLog
	{
        [Key, Identity]
        public int Id { get; set; }

		public string JobName { get; set; }
		public DateTime CreatedAtUTC { get; set; }
	}
}
