﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Model.Elastic
{
    public class Property
    {
        public long PropertyId { get; set; }
        public string PropertyName { get; set; }
        //public string Address { get; set; }
    }
}
