﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Model.Elastic
{
    public class MatchDocument
    {
        public double? Score { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
