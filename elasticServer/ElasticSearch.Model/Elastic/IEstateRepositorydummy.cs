﻿using ElasticSearch.Model.Elastic;
using ElasticSearch.Model.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Services.Interfaces
{
    public interface IEstateRepositorydummy
    {
        Task<List<Estate>> GetNext();
        Task<bool> UpdateIndexFlag(long Id);
    }
}
