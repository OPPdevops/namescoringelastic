﻿using ElasticSearch.Model.Elastic;
using ElasticSearch.Model.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Services.Interfaces
{
    public interface IPropertyRepositorydummy
    {
        Task<List<Property>> GetNext();
        Task<bool> UpdateIndexFlag(long Id);
    }
}
