﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Model.Elastic
{
    public class Estate
    {
        public long EstateId { get; set; }
        public string EstateName { get; set; }
        public string Address { get; set; }
    }
}
