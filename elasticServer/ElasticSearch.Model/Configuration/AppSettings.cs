﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Model.Configuration
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public SmartyStreet SmartyStreet { get; set; }

    }

    public class SmartyStreet
    {
        public string Key { get; set; }
        public string Referer { get; set; }
        public string AuthId { get; set; }
        public string AuthToken { get; set; }
    }
}
