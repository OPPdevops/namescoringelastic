﻿using ElasticSearch.Model.Configuration;
using ElasticSearch.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ElasticSearch.API.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IElasticSearchService elasticSearch;
        private readonly IElasticSearchService customElasticSearch;

        public HomeController(ILogger<HomeController> logger, IElasticSearchService elasticSearch, IElasticSearchService customElasticSearch)
        {
            this.elasticSearch = elasticSearch;
            this.customElasticSearch = customElasticSearch;
        }

        public object Index(string query)
        {
            if (string.IsNullOrEmpty(query))
                return new {};

            //customElasticSearch.CreateIndex();
            //customElasticSearch.IndexData();

            //return Ok(customElasticSearch.SearchExact("CHARLES SCHWAB & CO INC"));
            //return Ok(customElasticSearch.SearchExact("CHARLES SCHWAB & COMPANY INCORPORATED"));
            //return Ok(customElasticSearch.SearchFuzzy("CHARLES SCHWAB & CO. INC"));
            //return Ok(customElasticSearch.SearchFuzzy(query));
            var output = customElasticSearch.SearchFuzzy(query);
            return new
            {
                respCode = (HttpStatusCode.OK),
                respMessage = "Success",
                details = output
            };
            //return Ok(customElasticSearch.SearchExact("PEPSICO HOLDINGS"));
            //return Ok(customElasticSearch.SearchFuzzy("PEPSICO HOLDINGS Ltd"));


            //elasticSearch.DeleteIndexData();
            //elasticSearch.IndexData();
            //elasticSearch.SearchExact("CHARLES SCHWAB");
            //return Ok(elasticSearch.SearchFuzzy(query));
            //return Ok(elasticSearch.SearchFuzzy("CARLES SCWAB"));
            //return Ok(elasticSearch.SearchFuzzy("CHARLES SCHWAB & CO INC"));
            //return Ok(elasticSearch.SearchFuzzy("Pepsico Holdings"));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
