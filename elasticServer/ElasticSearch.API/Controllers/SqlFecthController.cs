﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using ElasticSearch.Services.Repository.Base;
using Newtonsoft.Json;
using System.Net;
using System.Data.SqlClient;

namespace ElasticSearch.API.Controllers
{

    [Route("api/sql")]
    [ApiController]
    public class SqlFecthController : Controller
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        DbFactory myDbFactory = new DbFactory("Server=LAPTOP-IVARBBS9\\SQLEXPRESS;Database=test;User Id=sa;Password=sa1234;");


        [HttpGet]
        public object SqlFetch([FromQuery] string name = "")
        {
            SqlConnection conn = (SqlConnection)myDbFactory.OpenConnection();
            //IDbConnection connection = new SqlConnection("Server=LAPTOP-IVARBBS9\\SQLEXPRESS;Database=test;User Id=sa;Password=sa1234;");
            //connection.Open();



            //return new {};

            try
            {
                //sql connection object
                //using (SqlConnection conn = new SqlConnection(connString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"
                    use prime_nr
                    go


                    SELECT 
                    		m_ETPSI.[Id] as 'ETPSI_ID'
                    			,m_ETPSI.[PropertyId] as 'OPRA_PropId'		
                    		,m_ETPSI.[EstateId] as 'EstateId'																	
                    			,m_Estate_ETPSI.[EstateName] as 'EstateName'													

                    		,m_PropertyDataSource_ETPSI.[Name] as 'DataSource-Name'														
                    		,m_State_ETPSI.[Abbreviation] as 'State-Abbrev'												
                    			,m_State_ETPSI.[Name] as 'State-Name'															
                    			,m_vw_EstateNameText_MatchingName.[Name] as 'Matching_ESTATE_Name'		

                    			,m_vw_PropNameText_via_MatchingPropertyNameId.[Text] as 'Matching_PROPERTY_Name'						

                    			,m_ETPSI.[MatchStatusId] as 'MatchStatus_Id'															
                    					,m_ETPSI_CurrentMatchStatus.[Name] as 'MatchStatus_Name'											

                    			,m_Property.[PropertyDescription] as 'PropertyDescription'																		

                    			,m_Property.[PropertyValueMin] as 'PropertyValueMin'												
                    			,m_Property.[PropertyValueMax] as 'PropertyValueMax'															

                    			,m_ETPSI.[NameProbability] as 'ETPSI_NameProbability'
                    			,m_ETPSI.[AddressProbability] as 'ETPSI_AddressProbability'


                    			,m_Estate_ETPSI.CourtCaseKey


                    			from dbo.EstateToPropertySearchInfo m_ETPSI with (nolock)



                    			left join dbo.EstateToProperty m_ETP with (nolock) on m_ETP.id = m_ETPSI.[Id]

                    			left join dbo.AzureAdUser m_AzureAdUser_ETP_CreatedByUser with (nolock) on m_AzureAdUser_ETP_CreatedByUser.Id = m_ETP.CreatedByUserId

                    			left join dbo.EstateToPropertyStatus m_ETPSI_CurrentMatchStatus with (nolock) on m_ETPSI_CurrentMatchStatus.Id = m_ETPSI.[MatchStatusId]

                    			left join dbo.[PropertyStatus] m_ETPSI_CurrentPropertyStatus with (nolock) on m_ETPSI_CurrentPropertyStatus.id = m_ETPSI.[PropertyStatusId]

                    			left join dbo.[State] m_State_ETPSI with (nolock) on m_State_ETPSI.Id = m_ETPSI.StateId

                    			left join dbo.PropertyDataSource m_PropertyDataSource_ETPSI with (nolock) on m_PropertyDataSource_ETPSI.Id = m_ETPSI.DataSourceId
                    		left join dbo.Estate m_Estate_ETPSI with (nolock) on m_Estate_ETPSI.Id = m_ETPSI.EstateId
                    		left join dbo.Property m_Property with (nolock) on m_Property.Id = m_ETPSI.PropertyId

                    			left join dbo.AzureAdUser m_AzureAdUser_Property_CreatedByUser with (nolock) on m_AzureAdUser_Property_CreatedByUser.Id = m_Property.CreatedByUserId
                    			outer apply (select top 1 * from [dbo].[PropertyNameText] with (nolock) 
                    						where
                    							[PropertyNameText].[NameId] = m_ETP.MatchingPropertyNameId and [PropertyNameText].[PropertyId] = m_ETP.PropertyId
                    							) m_vw_PropNameText_via_MatchingPropertyNameId

                    			LEFT join [dbo].[PropertyOwnerAddressText] m_vw_PropOwnerAddress_via_MatchingPropertyAddressId with (nolock) 
                    				on m_vw_PropOwnerAddress_via_MatchingPropertyAddressId.AddressId = m_ETP.MatchingPropertyAddressId and m_vw_PropOwnerAddress_via_MatchingPropertyAddressId.PropertyId = m_ETP.PropertyID
                    				outer apply (select top 1 * from [dbo].[Compatibility_PropertyNameText] with (nolock) 
                    								where [Compatibility_PropertyNameText].[Id] = m_ETP.PropertyId
                    								) m_vw_Compatibility_PropertyNameText_for_ETPpropertyId
                    					outer apply (select top 1 
                    								[Id]
                    								,[PropertyId]
                    								,[AddressId]
                    								,[Attn]
                    								,[Line1]
                    								,[Line2]
                    								,[City]
                    								,[State]
                    								,[Zip]
                    								,ltrim(rtrim(replace(replace(replace(replace(replace(replace(
                    												Coalesce([PropertyOwnerAddressText].[Attn],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Line1],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Line2],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[City],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[State],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Zip],'')																								
                    												,'  ',' '),'  ',' '),'  ',' '),'  ',' '),'  ',' '),'  ',' '))) as [POA_Concat_All]
                    								from [dbo].[PropertyOwnerAddressText] with (nolock)
                    								where [PropertyOwnerAddressText].PropertyId = m_ETP.PropertyId
                    								order by len(ltrim(rtrim(replace(replace(replace(replace(replace(replace(
                    												Coalesce([PropertyOwnerAddressText].[Attn],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Line1],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Line2],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[City],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[State],'') + ' ' + 
                    												coalesce([PropertyOwnerAddressText].[Zip],'')																								
                    												,'  ',' '),'  ',' '),'  ',' '),'  ',' '),'  ',' '),'  ',' ')))) desc								

                    							) m_vw_PropOwnerAddress_via_PropertyId
                    					left join [dbo].[EstateNameText] m_vw_EstateNameText_MatchingName with (nolock) 
                    						on m_vw_EstateNameText_MatchingName.Id = m_ETP.[MatchingNameId] and m_vw_EstateNameText_MatchingName.EstateId = m_ETP.EstateId

                    					left join dbo.[OwnershipType] m_MatchingName_OwnershipType with (nolock) on m_MatchingName_OwnershipType.Id  = m_vw_EstateNameText_MatchingName.OwnershipTypeId

                    					left join dbo.[OwnershipSubtype] m_MatchingName_OwnershipsubType with (nolock) on m_MatchingName_OwnershipsubType.Id  = m_vw_EstateNameText_MatchingName.OwnershipSubtypeId



                    				outer apply (select top 1 * from [dbo].[EstateAddressText] with (nolock)
                    								where 
                    									[EstateAddressText].[addressid] = m_ETP.[MatchingAddressId] and [EstateAddressText].EStateId = m_ETP.EstateId
                    							) m_vw_EstateAddressText_MatchingAddresss

                    	where

                    		 m_vw_EstateNameText_MatchingName.[Name] like 'REPLACABLESTR%'
                      			and (m_ETPSI.[NameProbability] > convert(decimal(28,5),.97))
                    ";
                    query = query.Replace("REPLACABLESTR", name);
                    //string query = @"SELECT e.Id,e.name,e.grade,e.class
                    //                 FROM student e;";
                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    //conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    Console.WriteLine(Environment.NewLine + "Retrieving data from database..." + Environment.NewLine);
                    Console.WriteLine("Retrieved records:");
                    string output = this.sqlDatoToJson(dr);
                    //string[] arr = JsonConvert.DeserializeObject<string[]>(output);
                    //long empID;
                    //int empClass;
                    //string empName, empGrade;
                    //int[] Array = new int[6];
                    ////check if there are records
                    //int count=0;
                    return new
                    {
                        respCode = (HttpStatusCode.OK),
                        respMessage = "Success",
                        details = output,
                    };
                    //                while (dr.Read())
                    //                    {
                    //                        empID = dr.GetInt64(0);
                    //                        empName = dr.GetString(1);
                    //                        empGrade = dr.GetString(2);
                    //                        empClass = dr.GetInt32(3);

                    //                    //display retrieved record

                    //                    Console.WriteLine("{0},{1},{2},{3}", empID.ToString(), empName, empGrade, empClass);
                    //                    Object[] obj = {
                    //    new { key = "id", value = empID.ToString()},
                    //    new { key = "name", value = empName},
                    //    new { key = "grade", value = empGrade},
                    //    new { key = "class", value = empClass}
                    //};
                    //                    this.sqlDatoToJson();
                    //                    //                    Array[count] = obj;
                    //                    //                    count++;
                    //                };
                    //else
                    //{
                    //    Console.WriteLine("No data found.");
                    //}

                    //close data reader
                    //dr.Close();

                    //close connection
                    //conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }

            return new
            {
                respCode = (HttpStatusCode.NotFound),
                respMessage = "Error",
            };
        }

        private String sqlDatoToJson(SqlDataReader dataReader)
        {
            var dataTable = new DataTable();
            dataTable.Load(dataReader);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dataTable);
            return JSONString;
        }
    }
}
