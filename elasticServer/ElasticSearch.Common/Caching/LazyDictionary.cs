﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Common.Caching
{
    public class LazyDictionary<TKey, TValue> : ILazyDictionary<TKey, TValue>
    {
        private static Dictionary<TKey, TValue> dictionary;

        public LazyDictionary()
        {
            dictionary = new Dictionary<TKey, TValue>();
        }
        public LazyDictionary(IEnumerable<KeyValuePair<TKey, TValue>> keyValuePairs)
        {
            dictionary = new Dictionary<TKey, TValue>(keyValuePairs);
        }

        public void Add(TKey key, TValue value)
        {
            dictionary.Add(key, value);
        }

        public TValue TryGetValue(TKey key)
        {
            if (dictionary.TryGetValue(key, out TValue item))
                return item;

            return default(TValue);
        }

        public List<TValue> GetValues()
        {
            if (dictionary.Count > 0)
                return dictionary.Values.ToList();
            return null;
        }

        public int Count => dictionary.Count;

        //public TValue AddOrUpdate(TKey key, Lazy<TValue> value)
        //{
        //    var lazyResult = this.concurrentDictionary.AddOrUpdate(key, value, (_key, existingVal) => value);
        //    //(key, existingVal) =>
        //    //{
        //    //    //// If this delegate is invoked, then the key already exists.
        //    //    //// Here we make sure the config really is the same config we already have.
        //    //    //if (config != existingVal)
        //    //    //    throw new ArgumentException("Duplicate config names are not allowed: {0}.", config.Name);

        //    //    // Update any field that needs to be updated here.
        //    //    //existingVal.lastQueryDate = DateTime.Now;
        //    //    return existingVal;
        //    //});
        //    return lazyResult.Value;
        //}

        //public bool AddOrUpdate(TKey key, Lazy<TValue> newValue)
        //{
        //    if (!concurrentDictionary.TryGetValue(key, out var item))
        //        return concurrentDictionary.TryAdd(key, newValue);

        //    if(JsonConvert.SerializeObject(item.Value) != JsonConvert.SerializeObject(newValue.Value))
        //        return concurrentDictionary.TryUpdate(key, newValue, item);

        //    return false;
        //}
    }

    public interface ILazyDictionary<TKey, TValue>
    {
        void Add(TKey key, TValue value);
        TValue TryGetValue(TKey key);

        List<TValue> GetValues();
        int Count { get; }
    }
}