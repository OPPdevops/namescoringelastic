﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Common.Scheduler.Cron
{
    [Serializable]
    public enum CrontabFieldKind
    {
        Minute,
        Hour,
        Day,
        Month,
        DayOfWeek
    }
}