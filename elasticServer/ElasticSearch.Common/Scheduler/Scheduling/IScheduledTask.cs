﻿using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Common.Scheduler.Scheduling
{
    public interface IScheduledTask
    {
        string Schedule { get; }
        Task ExecuteAsync(CancellationToken cancellationToken);
    }
}
