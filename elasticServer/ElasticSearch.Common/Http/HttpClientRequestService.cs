﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Text;

namespace ElasticSearch.Common.Http
{
    public class HttpClientRequestService : IHttpClientRequestService
    {
        private readonly ILogger<HttpClientRequestService> logger;
        private readonly IHttpClientFactory httpClientFactory;

        public HttpClientRequestService(ILogger<HttpClientRequestService> logger, IHttpClientFactory httpClientFactory)
        {
            this.logger = logger;
            this.httpClientFactory = httpClientFactory;
        }

        private string GetUriParameters(List<KeyValuePair<string, string>> parameters)
        {
            string uriParameters = string.Empty;

            if (parameters == null)
                return uriParameters;

            var paramsList = parameters.Select(param => string.Format("{0}={1}", param.Key, param.Value));
            uriParameters = string.Join("&", paramsList);

            return uriParameters;
        }

        public string GetUriParameters(IEnumerable<string> dataStringList, string paramName)
        {
            string uriParameters = string.Empty;
            if (dataStringList != null)
            {
                List<KeyValuePair<string, string>> parameters = dataStringList.Select(t => new KeyValuePair<string, string>(paramName, t)).ToList();
                uriParameters = GetUriParameters(parameters);
                uriParameters = (!string.IsNullOrEmpty(uriParameters)) ? $"&{uriParameters}" : "";
            }
            return uriParameters;
        }

        public T GetResponseObject<T>(string uri)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = httpClient.GetAsync(uri).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }
        public T GetResponseObject<T>(string uri, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = httpClient.GetAsync(uri).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> GetResponseObjectAsync<T>(string uri)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = await httpClient.GetAsync(uri);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }
        public async Task<T> GetResponseObjectAsync<T>(string uri, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }
                var response = await httpClient.GetAsync(uri);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public T PostResponseObject<T>(string uri, object requestObject)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var json = JsonConvert.SerializeObject(requestObject);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                var response = httpClient.PostAsync(uri, content).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> PostResponseObjectAsync<T>(string uri, object requestObject)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var json = JsonConvert.SerializeObject(requestObject);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                var response = await httpClient.PostAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> PostResponseObjectAsync<T>(string uri, HttpContent content, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = await httpClient.PostAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure posting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    logger.LogError(message);
                    //Not throwing error from here as we need the response object at caller
                    //throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<HttpResponseMessage> PostResponseObjectAsync(string uri, StringContent content, List<KeyValuePair<string, string>> headers = null)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = await httpClient.PostAsync(uri, content);
                var responseMessage = response.EnsureSuccessStatusCode();

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure posting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }
                return responseMessage;
            }
        }

        public T PutResponseObject<T>(string uri, object requestObject)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var json = JsonConvert.SerializeObject(requestObject);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                var response = httpClient.PutAsync(uri, content).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> PutResponseObjectAsync<T>(string uri, object requestObject)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var json = JsonConvert.SerializeObject(requestObject);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                var response = await httpClient.PutAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure getting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> PutResponseObjectAsync<T>(string uri, HttpContent content, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = await httpClient.PutAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure posting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    logger.LogError(message);
                    //Not throwing error from here as we need the response object at caller
                    //throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<HttpResponseMessage> PutResponseObjectAsync(string uri, StringContent content, List<KeyValuePair<string, string>> headers = null)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = await httpClient.PutAsync(uri, content);
                var responseMessage = response.EnsureSuccessStatusCode();

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure posting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }
                return responseMessage;
            }
        }
        public T DeleteResponseObject<T>(string uri)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = httpClient.DeleteAsync(uri).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure deleting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }
        public T DeleteResponseObject<T>(string uri, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }

                var response = httpClient.DeleteAsync(uri).Result;

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure deleting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = response.Content.ReadAsStringAsync().Result;
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }

        public async Task<T> DeleteResponseObjectAsync<T>(string uri)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                var response = await httpClient.DeleteAsync(uri);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure deleting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }
        public async Task<T> DeleteResponseObjectAsync<T>(string uri, List<KeyValuePair<string, string>> headers)
        {
            using (var httpClient = httpClientFactory.CreateClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (headers != null)
                {
                    foreach (var item in headers)
                    {
                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }
                var response = await httpClient.DeleteAsync(uri);

                if (!response.IsSuccessStatusCode)
                {
                    string message = string.Format("Failure deleting object. Status: {0}. Reason: {1}. URI: {2}", response.StatusCode, response.ReasonPhrase, uri);
                    throw new HttpRequestException(message);
                }

                var data = await response.Content.ReadAsStringAsync();
                T responseObject = ConvertResponseObject<T>(data);

                return responseObject;
            }
        }
        private T ConvertResponseObject<T>(string data)
        {
            T responseObject = default(T);
            try
            {
                if (typeof(T) == typeof(string))
                    responseObject = (T)(object)data;
                else
                    responseObject = JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error deserializing object: {typeof(T)}");
            }
            return responseObject;
        }
    }
}