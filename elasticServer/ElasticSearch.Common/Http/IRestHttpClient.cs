﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Common.Http
{
    public interface IRestHttpClient
    {
        Task<string> PostAsync(string url, object input);
        Task<TResult> PostAsync<TResult>(string url, object input);
        Task<TResult> GetAsync<TResult>(string url);
        Task<string> GetAsync(string url);
        Task<string> PutAsync(string url, object input);
        Task<string> PutAsync(string url, HttpContent content);
        Task<string> DeleteAsync(string url);

    }
}
