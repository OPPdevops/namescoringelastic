﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace ElasticSearch.Common.Http
{
    public static class HttpClientServiceExtensions
    {
        public static IServiceCollection AddRestHttpClient(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddHttpClient();
            services.AddSingleton<IRestHttpClient, RestHttpClient>();
            services.AddSingleton<IHttpClientRequestService, HttpClientRequestService>();

            return services;
        }
    }
}
