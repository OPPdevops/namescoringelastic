﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Common.Http
{
    public interface IHttpClientRequestService
    {
        string GetUriParameters(IEnumerable<string> dataStringList, string paramName);
        T GetResponseObject<T>(string uri);
        T GetResponseObject<T>(string uri, List<KeyValuePair<string, string>> headers);
        Task<T> GetResponseObjectAsync<T>(string uri);
        Task<T> GetResponseObjectAsync<T>(string uri, List<KeyValuePair<string, string>> headers);
        T PostResponseObject<T>(string uri, object requestObject);
        Task<T> PostResponseObjectAsync<T>(string uri, object requestObject);
        Task<T> PostResponseObjectAsync<T>(string uri, HttpContent content, List<KeyValuePair<string, string>> headers);
        Task<HttpResponseMessage> PostResponseObjectAsync(string uri, StringContent content, List<KeyValuePair<string, string>> headers = null);
        T PutResponseObject<T>(string uri, object requestObject);
        Task<T> PutResponseObjectAsync<T>(string uri, object requestObject);
        Task<T> PutResponseObjectAsync<T>(string uri, HttpContent content, List<KeyValuePair<string, string>> headers);
        Task<HttpResponseMessage> PutResponseObjectAsync(string uri, StringContent content, List<KeyValuePair<string, string>> headers = null);
    }
}
