﻿using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Common.HealthCheck
{
    public interface ISupportHealthcheck
    {
        string ComponentName { get; }
        Task<HealthcheckResult> PerformHealthcheck(CancellationToken cancellationToken = default);
    }
}
