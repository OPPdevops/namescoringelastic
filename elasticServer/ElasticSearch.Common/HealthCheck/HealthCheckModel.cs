﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Common.HealthCheck
{
    public class HealthCheckModel
    {
        public string ComponentName { get; set; }
        public bool IsReady { get; set; } = true;
        public bool IsHealthy { get; set; }
        public string MachineName { get; set; }
        public string EnvironmentName { get; set; }
        public string HealthDescription { get; set; }
        public object Detail { get; set; }

        public HealthCheckModel()
        {
            MachineName = Environment.MachineName;
        }
    }
}
