﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Common.HealthCheck
{
    public static class HealthCheckExtension
    {
        public static IServiceCollection AddAPIHealthCheck(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddSingleton<ISupportHealthcheck, ProcessHealthCheck>();
            services.AddSingleton<IHealthCheckService, HealthCheckService>();

            return services;
        }
    }
}
