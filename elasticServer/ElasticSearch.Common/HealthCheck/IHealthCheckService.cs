﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Common.HealthCheck
{
    public interface IHealthCheckService
    {
        string ComponentName { get; }
        Task<HealthCheckModel> PerformHealthcheck(CancellationToken cancellationToken = default);
    }
}
