﻿using ElasticSearch.Common.Scheduler.Scheduling;
using ElasticSearch.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Services
{
    public class PropertyBackgroundJob : IScheduledTask
    {
        //minute hour month day weekday
        //public string Schedule => "1 * * * *"; //Every hour
        public string Schedule => "* * * * *"; //Every minute

        private readonly ILogger<PropertyBackgroundJob> logger;
        private readonly IElasticSearchService elasticSearchService;
        private readonly IPropertyRepository propertyRepository;
        private readonly IJobExecutionLogRepository jobExecutionLogRepository;

        public PropertyBackgroundJob(ILogger<PropertyBackgroundJob> logger, IElasticSearchService elasticSearchService,
            IPropertyRepository propertyRepository, IJobExecutionLogRepository jobExecutionLogRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.elasticSearchService = elasticSearchService ?? throw new ArgumentNullException(nameof(elasticSearchService));
            this.jobExecutionLogRepository = jobExecutionLogRepository ?? throw new ArgumentNullException(nameof(jobExecutionLogRepository));
            this.propertyRepository = propertyRepository ?? throw new ArgumentNullException(nameof(propertyRepository));
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            //for (int loop = 0; loop < 10; loop++)
            //{
                try
                {
                    var dataToIndex = await propertyRepository.GetNext();
                    if (dataToIndex.Any())
                    {
                        elasticSearchService.IndexDataProperty(dataToIndex);
                        await propertyRepository.UpdateIndexFlag(dataToIndex.Max(x => x.PropertyId));
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex?.Message ?? $"Error while Indexing Data. Method: PropertyBackgroundJob.ExecuteAsync", ex);
                }
            //}


            //Property Job Execution Log
            //await jobExecutionLogRepository.AddAsync(nameof(PropertyBackgroundJob));

            await Task.Delay(500, cancellationToken).ConfigureAwait(false);
        }
    }
}

