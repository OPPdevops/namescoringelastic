﻿using ElasticSearch.Common.Scheduler.Scheduling;
using ElasticSearch.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearch.Services
{
    public class EstateBackgroundJob : IScheduledTask
    {
        //minute hour month day weekday
        //public string Schedule => "1 * * * *"; //Every hour
        public string Schedule => "*/30 * * * *"; //Every minute

        private readonly ILogger<EstateBackgroundJob> logger;
        private readonly IElasticSearchService elasticSearchService;
        private readonly IEstateRepository estateRepository;
        private readonly IJobExecutionLogRepository jobExecutionLogRepository;

        public EstateBackgroundJob(ILogger<EstateBackgroundJob> logger, IElasticSearchService elasticSearchService,
            IEstateRepository estateRepository, IJobExecutionLogRepository jobExecutionLogRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.elasticSearchService = elasticSearchService ?? throw new ArgumentNullException(nameof(elasticSearchService));
            this.jobExecutionLogRepository = jobExecutionLogRepository ?? throw new ArgumentNullException(nameof(jobExecutionLogRepository));
            this.estateRepository = estateRepository ?? throw new ArgumentNullException(nameof(estateRepository));
        }

        public async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            //for (int loop = 0; loop < 10; loop++)
            //{
                try
                {
                    var dataToIndex = await estateRepository.GetNext();
                    if (dataToIndex.Any())
                    {
                        elasticSearchService.IndexData(dataToIndex);
                        await estateRepository.UpdateIndexFlag(dataToIndex.Max(x => x.EstateId));
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex?.Message ?? $"Error while Indexing Data. Method: EstateBackgroundJob.ExecuteAsync", ex);
                }
            //}


            //Estate Job Execution Log
            //await jobExecutionLogRepository.AddAsync(nameof(EstateBackgroundJob));

            await Task.Delay(500, cancellationToken).ConfigureAwait(false);
        }
    }
}

