﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ElasticSearch.Services.Repository.Base
{
    public class BaseRepository<T> : DapperRepository<T> where T : class
    {
        private readonly IDbFactory _factory;
        public BaseRepository(IDbFactory factory, ISqlGenerator<T> generator)
            : base(factory.OpenConnection(), generator)
        {
            _factory = factory;
        }

        protected IDbConnection GetConnection()
        {
            return _factory.OpenConnection();
        }
    }
}
