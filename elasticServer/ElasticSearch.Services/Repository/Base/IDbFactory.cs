﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ElasticSearch.Services.Repository.Base
{
    public interface IDbFactory
    {
        IDbConnection OpenConnection();
    }
}
