﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ElasticSearch.Services.Repository.Base
{
    public class DbFactory : IDbFactory, IDisposable
    {
        private readonly string connectionString;

        public DbFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IDbConnection OpenConnection()
        {
            IDbConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public void Dispose()
        {
            OpenConnection().Dispose();
        }
    }
}
