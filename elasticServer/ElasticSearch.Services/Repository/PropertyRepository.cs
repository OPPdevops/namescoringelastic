﻿using Dapper;
using ElasticSearch.Model.Elastic;
using ElasticSearch.Model.Entity;
using ElasticSearch.Services.Interfaces;
using ElasticSearch.Services.Repository.Base;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using ElasticSearch.Common.Extensions;
using static MicroOrm.Dapper.Repositories.SqlGenerator.Filters.OrderInfo;

namespace ElasticSearch.Services.Repository
{

    public class PropertyRepository : BaseRepository<PropertyName>, IPropertyRepository
    {
        private readonly ILogger<PropertyRepository> logger;
        public PropertyRepository(IDbFactory factory, ISqlGenerator<PropertyName> sqlGenerator, ILogger<PropertyRepository> logger)
            : base(factory, sqlGenerator)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<List<Property>> GetNext()
        {
            try
            {
                var lastId = await GetLastIndexedId();
                var data = await base.SetLimit(100000u)
                                     .SetOrderBy(0, a => a.StatePropertyId)
                                     .FindAllAsync(a => a.StatePropertyId > lastId);
                Console.WriteLine("data{0}",data);
                return data.AsList().Select(e => new Property
                {
                    PropertyId = e.StatePropertyId,
                    PropertyName = e.Name
                }).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while getting data. Method: GetNext", ex);
                return new List<Property>();
            }
        }

        public async Task<bool> UpdateIndexFlag(long Id)
        {
            try
            {
                string sql = $@"UPDATE [dbo].[IndexCounter2] SET Id = {Id}";

                return await Connection.ExecuteAsync(sql) > 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while updating Index Flags. Method: Update Id", ex);
                return false;
            }
        }

        private async Task<long> GetLastIndexedId()
        {
            try
            {
                string sql = @"SELECT Id FROM [dbo].[IndexCounter2]";

                return await Connection.ExecuteScalarAsync<long>(sql);
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while updating Index Flags. Method: Update Id", ex);
                return 0;
            }
        }
    }
}
