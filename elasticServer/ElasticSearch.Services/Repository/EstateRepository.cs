﻿using Dapper;
using ElasticSearch.Model.Elastic;
using ElasticSearch.Model.Entity;
using ElasticSearch.Services.Interfaces;
using ElasticSearch.Services.Repository.Base;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using ElasticSearch.Common.Extensions;
using static MicroOrm.Dapper.Repositories.SqlGenerator.Filters.OrderInfo;

namespace ElasticSearch.Services.Repository
{

    public class EstateRepository : BaseRepository<EstateName>, IEstateRepository
    {
        private readonly ILogger<EstateRepository> logger;
        public EstateRepository(IDbFactory factory, ISqlGenerator<EstateName> sqlGenerator, ILogger<EstateRepository> logger)
            : base(factory, sqlGenerator)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<List<Estate>> GetNext()
        {
            try
            {
                var lastId = await GetLastIndexedId();
                var data = await base.SetLimit(100000u)
                                     .SetOrderBy(SortDirection.ASC, a => a.Id)
                                     .FindAllAsync(a => a.Id > lastId);

                return data.AsList().Select(e => new Estate
                {
                    EstateId = e.Id,
                    EstateName = e.Name
                }).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while getting data. Method: GetNext", ex);
                return new List<Estate>();
            }
        }

        public async Task<bool> UpdateIndexFlag(long Id)
        {
            try
            {
                string sql = $@"UPDATE [dbo].[IndexCounter] SET Id = {Id}";

                return await Connection.ExecuteAsync(sql) > 0;
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while updating Index Flags. Method: Update Id", ex);
                return false;
            }
        }

        private async Task<long> GetLastIndexedId()
        {
            try
            {
                string sql = @"SELECT Id FROM [dbo].[IndexCounter]";

                return await Connection.ExecuteScalarAsync<long>(sql);
            }
            catch (Exception ex)
            {
                logger.LogError(ex?.Message ?? $"Error while updating Index Flags. Method: Update Id", ex);
                return 0;
            }
        }
    }
}
