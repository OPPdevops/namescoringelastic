﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Services.Interfaces
{
    public interface IJobExecutionLogRepository
    {
        Task<bool> AddAsync(string jobName);
    }
}
